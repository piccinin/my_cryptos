import { AtivoServices } from '@/domain/services';
import {
  CreateService,
  DeleteService,
  UpdateService,
  FindAllIdNameService,
  FindAllService
} from '@/domain/protocols';
import {
  AtivoUpdateQuotes
} from '@/domain/protocols/ativos';

export const makeCreateServices = (): CreateService => {
  return new AtivoServices();
};

export const makeDeleteService = (): DeleteService => {
  return new AtivoServices();
};

export const makeUpdateService = (): UpdateService => {
  return new AtivoServices();
};

export const makeFindAllIdNameService = (): FindAllIdNameService => {
  return new AtivoServices();
};

export const makeFindAllService = (): FindAllService => {
  return new AtivoServices();
};

export const makeAtivoUpdateQuotes = (): AtivoUpdateQuotes => {
  return new AtivoServices();
};
