export interface Usuario {
  id: number
  email: string
  nomeCompleto?: string
  usuario?: string
  senha?: string
  investimento_inicial?: number
  createdAt?: Date
  updatedAt?: Date
  picture?: string
  given_name?: string
  family_name?: string
  credential?: string
  json_data?: string
  ativo?: string
  auth_token?: string
  auth_token_expiration?: Date
  auth_token_expiration_time?: number
  data_ativacao?: Date
  data_desativacao?: Date
  data_autorizacao?: Date
}
