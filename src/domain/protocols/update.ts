export interface UpdateService {
  update: (entity: any) => Promise<boolean>
}
