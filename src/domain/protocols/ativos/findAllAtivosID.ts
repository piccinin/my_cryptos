import { Ativo } from '@/domain/entities';
export interface AtivoFindAllAtivosID {
  findAllAtivosID: () => Promise<Ativo[]>
}
