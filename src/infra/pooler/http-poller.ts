import { CoinMarketCapGateway } from '@/infra/gateways/CoinMarketCapGateway';
import { AtivoServices } from '@/domain/services';
import Logger from '@/infra/logs/Logger';
import { Config } from '@/shared/types';
import { Ativo } from '@/domain/entities/Ativo';

export default class HttpPoller {
  private intervalFunctionID: any;
  private readonly config: Config;
  readonly PRICE_UPDATE_INTERVAL = 30 * 60 * 1000;

  constructor (config: Config) {
    this.config = config;
  }

  public setTintervalFunctionID (intervalFunctionID: any): void {
    this.intervalFunctionID = intervalFunctionID;
  }

  public getTintervalFunctionID (): any {
    return this.intervalFunctionID;
  }

  public async updateQuotes (): Promise<string> {
    try {
      const cmcCconfigs = this.config.coinMarketCap;
      const ativoServices = new AtivoServices();
      const ativos = await ativoServices.findAllAtivosID();

      let ativosID = '';
      ativos.forEach(function name (elemnt: Ativo) {
        ativosID += elemnt.id.toString() + ',';
      });
      // "1,2,1027,1762,1839,1975,2010,2424,3992,5426,6951,8644,9638";
      ativosID = ativosID.substring(0, ativosID.lastIndexOf(','));

      const cmcServices = new CoinMarketCapGateway(cmcCconfigs);
      const coinsPrices = await cmcServices.loadCoinsPrices(ativosID);
      await ativoServices.priceUpdate(coinsPrices);
      Logger.infoLog('Preços Atualizados');
      return 'ok';
    } catch (error) {
      Logger.errorLog(error);
      throw error;
    }
  }

  private updateQuotesAsync (): void {
    void this.updateQuotes();
  }

  public async start (): Promise<void> {
    this.setTintervalFunctionID(
      setInterval(this.updateQuotesAsync, this.PRICE_UPDATE_INTERVAL)
    );
  }

  public stop (): void {
    clearInterval(this.getTintervalFunctionID());
  }
}
