import { Controller } from '@/presentation/protocols/controller';

import {
  CreateTradeController,
  DeleteTradeController,
  FindTradeController
} from '@/presentation/controllers/trades';

import { TradeServices } from '@/domain/services';

export const makeCreateTradeController = (): Controller => {
  return new CreateTradeController(new TradeServices());
};

export const makeDeleteTradeController = (): Controller => {
  return new DeleteTradeController(new TradeServices());
};

export const makeFindTradeController = (operacao: string): Controller => {
  return new FindTradeController(new TradeServices(), operacao);
};
