'use strict';

import { Model } from 'sequelize';

interface AtivoAttributes {
  id: number
  codigo: string
  nome: string
  preco: number
  logo: string
  twitter: string
  stopLoss: number
  marketCap: number
  alvo: number
  createdAt: Date
  updatedAt: Date
}

// eslint-disable-next-line @typescript-eslint/explicit-function-return-type
export default (sequelize: any, DataTypes: any) => {
  class Ativo extends Model<AtivoAttributes> implements AtivoAttributes {
    id: number;
    codigo: string;
    nome: string;
    preco: number;
    logo: string;
    twitter: string;
    stopLoss: number;
    marketCap: number;
    alvo: number;
    createdAt: Date;
    updatedAt: Date;
    static associate (models: any): void {
      Ativo.hasMany(models.Trade);
    }
  }

  Ativo.init({
    id: {
      type: DataTypes.BIGINT,
      allowNull: false,
      autoIncrement: true,
      primaryKey: true
    },
    codigo: {
      type: DataTypes.STRING(5),
      allowNull: false
    },
    nome: {
      type: DataTypes.STRING(50),
      allowNull: false
    },
    preco: {
      type: DataTypes.FLOAT,
      allowNull: false
    },
    logo: {
      type: DataTypes.STRING,
      allowNull: true
    },
    twitter: {
      type: DataTypes.STRING,
      allowNull: true
    },
    stopLoss: {
      type: DataTypes.DOUBLE,
      allowNull: true
    },
    marketCap: {
      type: DataTypes.DOUBLE,
      allowNull: true
    },
    alvo: {
      type: DataTypes.DOUBLE,
      allowNull: true
    },
    createdAt: {
      type: DataTypes.DATE,
      allowNull: true
    },
    updatedAt: {
      type: DataTypes.DATE,
      allowNull: true
    }
  }, {
    sequelize,
    tableName: 'ativos',
    modelName: 'Ativo'
  });

  return Ativo;
};
