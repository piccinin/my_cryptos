import { Application } from 'express';
import { adaptRoute } from '@/main/adapters/';
import {
  makeCreateCarteiraController,
  makeUpdateCarteiraController,
  makeDeleteCarteiraController,
  makeFindCarteiraController
} from '@/main/factories/controllers/makeCarteirasControllers';

export default async (app: Application): Promise<void> => {
  app.post('/api/carteira', adaptRoute(makeCreateCarteiraController()))
    .patch('/api/carteira', adaptRoute(makeUpdateCarteiraController()))
    .delete('/api/carteira', adaptRoute(makeDeleteCarteiraController()));

  app.get('/api/carteira/listAll',
    adaptRoute(makeFindCarteiraController('listAll')));

  app.get('/api/carteira/listAllIdName',
    adaptRoute(makeFindCarteiraController('listAllIdName')));

  app.get('/api/carteira/getSaldoDolar',
    adaptRoute(makeFindCarteiraController('getSaldoDolar')));

  app.get('/api/carteira/getSaldoDolarCarteira',
    adaptRoute(makeFindCarteiraController('getSaldoDolarCarteira')));

  app.get('/api/carteira/getInvestimentoInicial',
    adaptRoute(makeFindCarteiraController('getInvestimentoInicial')));
};
