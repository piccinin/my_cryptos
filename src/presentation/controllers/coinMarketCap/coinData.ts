import { Controller, HttpResponse } from '@/presentation/protocols';
import { serverError, badRequest, ok } from '@/presentation/helpers';
import { MissingParamsError } from '@/presentation/errors/';
import { CoinMarketCapGateway } from '@/infra/gateways/CoinMarketCapGateway';
import { Ativo } from '@/domain/entities/Ativo';

export class CoinsDataController implements Controller {
  constructor (
    readonly coinMarketCapGateway: CoinMarketCapGateway
  ) {}

  async handle (request: CoinsDataController.Request): Promise<HttpResponse> {
    try {
      for (const field of ['codigo']) {
        if (!Object.prototype.hasOwnProperty.call(request, field)) {
          return badRequest(new MissingParamsError(field));
        }
      }

      const coins: any = await this.coinMarketCapGateway.getCoinIdBySymbol(request.codigo);
      const ativos: Ativo[] = [];
      for (const coin of coins) {
        const coinInfo = await this.coinMarketCapGateway.getCoinInfoById(coin.id);
        ativos.push({
          id: coinInfo[coin.id].id,
          nome: coinInfo[coin.id].name,
          logo: coinInfo[coin.id].logo,
          twitter: coinInfo[coin.id].urls.twitter
        });
      }
      return ok(ativos);
    } catch (error: any) {
      return serverError(error);
    }
  }
}

export namespace CoinsDataController {
  export type Request = {
    codigo: string
  };
}
