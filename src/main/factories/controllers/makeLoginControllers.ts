import { Controller } from '@/presentation/protocols/controller';
import { LoginController } from '@/presentation/controllers/auth';
import { LoginServices } from '@/domain/services';

export const makeLoginController = (): Controller => {
  return new LoginController(new LoginServices());
};
