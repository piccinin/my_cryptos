import { DeleteCarteiraController } from "@/presentation/controllers/carteiras";
import { CarteiraServices } from '@/domain/services'

type SutTypes = {
  sut: DeleteCarteiraController
  carteiraServicesStub: CarteiraServices
}

class CarteiraServicesStub extends CarteiraServices {
  public async delete(entity: any): Promise<any> {
      return true;
  }
}

const makeSut = (): SutTypes => {
  const carteiraServicesStub = new CarteiraServicesStub();
  const sut = new DeleteCarteiraController(carteiraServicesStub);
  return {
    sut,
    carteiraServicesStub
  }
}

const makeHttpRequestDataMock = (): DeleteCarteiraController.Request => ({
    id: 1
})

describe('DeleteCarteiraController', () => {
  it('should return HTTP 400 error for missing param id', async () =>{
    const { sut } = makeSut();
    const httpRquest = makeHttpRequestDataMock();
    delete httpRquest.id;
    const httpResponse = await sut.handle(httpRquest);
    expect(httpResponse.statusCode).toBe(400);
  })

  it('should return HTTP 500 if an internal server error happen', async () =>{
    const { sut, carteiraServicesStub } = makeSut();
    const httpRquest = makeHttpRequestDataMock();

    jest.spyOn(carteiraServicesStub, 'delete')
    .mockReturnValue(Promise.resolve(false));

    const httpResponse = await sut.handle(httpRquest);
    expect(httpResponse.statusCode).toBe(500);
  })

  it('should return HTTP 200 if delete carteira service was corretly called', async () =>{
    const { sut, carteiraServicesStub } = makeSut();
    const httpRquest = makeHttpRequestDataMock();

    const httpResponse = await sut.handle(httpRquest);
    expect(httpResponse.statusCode).toBe(200);
  })


})