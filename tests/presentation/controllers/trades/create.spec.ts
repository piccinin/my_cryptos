import { CreateTradeController } from "@/presentation/controllers/trades";
import { TradeServices } from '@/domain/services'

type SutTypes = {
  sut: CreateTradeController
  tradeServicesStub: TradeServices
}

class TradeServicesStub extends TradeServices {
  public async create(entity: any): Promise<any> {
      return false;
  }
}

const makeSut = (): SutTypes => {
  const tradeServicesStub = new TradeServicesStub();
  const sut = new CreateTradeController(tradeServicesStub);
  return {
    sut,
    tradeServicesStub
  }
}

const makeHttpRequestDataDummy = (): CreateTradeController.Request => ({
  AtivoId: 1,
  CarteiraId: 1,
  tp_trans: 'n',
  qtde: 1,
  preco_ativo: 1,
  valor: 1
})

describe('CreateTradeController', () => {
  it('should return HTTP 400 error for missing param AtivoId', async () =>{
    const { sut } = makeSut();
    const httpRquest = makeHttpRequestDataDummy();
    delete httpRquest.AtivoId;
    const httpResponse = await sut.handle(httpRquest);
    expect(httpResponse.statusCode).toBe(400);
  })

  it('should return HTTP 400 error for missing param CarteiraId', async () =>{
    const { sut } = makeSut();
    const httpRquest = makeHttpRequestDataDummy();
    delete httpRquest.CarteiraId;
    const httpResponse = await sut.handle(httpRquest);
    expect(httpResponse.statusCode).toBe(400);
  })

  it('should return HTTP 400 error for missing param tp_trans', async () =>{
    const { sut } = makeSut();
    const httpRquest = makeHttpRequestDataDummy();
    delete httpRquest.tp_trans;
    const httpResponse = await sut.handle(httpRquest);
    expect(httpResponse.statusCode).toBe(400);
  })

  it('should return HTTP 400 error for missing param qtde', async () =>{
    const { sut } = makeSut();
    const httpRquest = makeHttpRequestDataDummy();
    delete httpRquest.qtde;
    const httpResponse = await sut.handle(httpRquest);
    expect(httpResponse.statusCode).toBe(400);
  })
  it('should return HTTP 400 error for missing param preco_ativo', async () =>{
    const { sut } = makeSut();
    const httpRquest = makeHttpRequestDataDummy();
    delete httpRquest.preco_ativo;
    const httpResponse = await sut.handle(httpRquest);
    expect(httpResponse.statusCode).toBe(400);
  })

  it('should return HTTP 400 error for missing param valor', async () =>{
    const { sut } = makeSut();
    const httpRquest = makeHttpRequestDataDummy();
    delete httpRquest.valor;
    const httpResponse = await sut.handle(httpRquest);
    expect(httpResponse.statusCode).toBe(400);
  })

  it('should return HTTP 500 if an internal server error happen', async () =>{
    const { sut, tradeServicesStub } = makeSut();
    const httpRquest = makeHttpRequestDataDummy();

    jest.spyOn(tradeServicesStub, 'create')
    .mockReturnValue(Promise.resolve(false));

    const httpResponse = await sut.handle(httpRquest);

    expect(httpResponse.statusCode).toBe(500);
  })

  it('should return HTTP 200 if create trade service was corretly called', async () =>{
    const { sut, tradeServicesStub } = makeSut();
    const httpRquest = makeHttpRequestDataDummy();

    jest.spyOn(tradeServicesStub, 'create')
    .mockReturnValueOnce(Promise.resolve(true));

    const httpResponse = await sut.handle(httpRquest);
    expect(httpResponse.statusCode).toBe(200);
  })


})