import { badRequest, ok, serverError, unauthorizedError } from '@/presentation/helpers';
import { Controller, HttpResponse } from '@/presentation/protocols';
import { UsuarioServices } from '@/domain/services';
import { MissingParamsError } from '@/presentation/errors';

export class AuthorizeController implements Controller {
  constructor (
    readonly usuarioServices: UsuarioServices
  ) {}

  async handle (request: AuthorizeController.Request): Promise<HttpResponse> {
    try {
      for (const field of ['auth_token']) {
        if (!Object.prototype.hasOwnProperty.call(request, field)) {
          return badRequest(new MissingParamsError(field));
        }
      }

      if (await this.usuarioServices.isAuthorized(request.auth_token)) {
        return ok('ok');
      } else {
        return unauthorizedError('unauthorized');
      }
    } catch (error: any) {
      return serverError(error);
    }
  }
}

export namespace AuthorizeController {
  export type Request = {
    auth_token: string
  };
}
