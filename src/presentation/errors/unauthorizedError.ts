export class UnauthorizedError extends Error {
  constructor (error: string) {
    super(error);
    this.name = 'UnauthorizedError';
  }
}
