const getConfig = require('./config');

const getDatabases = async () => {
  const config = await getConfig();
  return config.databases;
};

module.exports = getDatabases();
