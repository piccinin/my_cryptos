import { UsuarioServices } from '@/domain/services';

const freeRoutes = ['/login', '/authorize'];

export default async function (req: any, res: any, next: any): Promise<any> {
  try {
    if (freeRoutes.includes(req.path)) {
      next();
    } else {
      if (req.headers['x-access-token']) {
        const usuarioServices = new UsuarioServices();
        const authorized = await usuarioServices.isAuthorized(req.headers['x-access-token']);

        if (authorized) {
          next();
        } else {
          res.status(401).json('unauthorized');
        }
      } else {
        res.status(401).json('unauthorized');
      }
    }
  } catch (error) {
    res.status(401).json('unauthorized');
  }
}
