import { CreateCarteiraController } from "@/presentation/controllers/carteiras";
import { CarteiraServices } from '@/domain/services'

type SutTypes = {
  sut: CreateCarteiraController
  carteiraServicesStub: CarteiraServices
}

class CarteiraServicesStub extends CarteiraServices {
  public async create(entity: any): Promise<any> {
      return false;
  }
}

const makeSut = (): SutTypes => {
  const carteiraServicesStub = new CarteiraServicesStub();
  const sut = new CreateCarteiraController(carteiraServicesStub);
  return {
    sut,
    carteiraServicesStub
  }
}

const makeHttpRequestDataMock = (): CreateCarteiraController.Request => ({
  nome: 'any_name'
})

describe('CreateCarteiraController', () => {
  it('should return HTTP 400 error for missing param nome', async () =>{
    const { sut } = makeSut();
    const httpRquest = makeHttpRequestDataMock();
    delete httpRquest.nome;
    const httpResponse = await sut.handle(httpRquest);
    expect(httpResponse.statusCode).toBe(400);
  })

  it('should return HTTP 500 if an internal server error happen', async () =>{
    const { sut, carteiraServicesStub } = makeSut();
    const httpRquest = makeHttpRequestDataMock();

    jest.spyOn(carteiraServicesStub, 'create')
    .mockReturnValue(Promise.resolve(false));

    const httpResponse = await sut.handle(httpRquest);

    expect(httpResponse.statusCode).toBe(500);
  })

  it('should return HTTP 200 if create carteira service was corretly called', async () =>{
    const { sut, carteiraServicesStub } = makeSut();
    const httpRquest = makeHttpRequestDataMock();

    jest.spyOn(carteiraServicesStub, 'create')
    .mockReturnValueOnce(Promise.resolve(true));

    const httpResponse = await sut.handle(httpRquest);
    expect(httpResponse.statusCode).toBe(200);
  })
})