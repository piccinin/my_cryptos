import { UsuarioServices } from '@/domain/services';

export class UsuarioController {
  private readonly usuarioServices: UsuarioServices;

  constructor () {
    this.usuarioServices = new UsuarioServices();
  }
}
