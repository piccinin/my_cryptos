import DB from '@/infra/db/models';
import { Ativo } from '@/domain/entities/Ativo';
import GenericRepository from '@/data/repository/GenericRepository';

export default class AtivoRepository extends GenericRepository {
  constructor () {
    super(DB.Ativo, DB.sequelize);
  }

  public async saveLote (cmcStocksTwitter: any): Promise<void> {
    Object.keys(cmcStocksTwitter.data).forEach((element: any) => {
      DB.Ativo.update({
        twitter: cmcStocksTwitter.data[element].urls.twitter[0]
      },
      {
        where: { id: cmcStocksTwitter.data[element].id }
      });
    });
  }

  public async priceUpdate (cmcStocksPrices: any): Promise<void> {
    Object.keys(cmcStocksPrices.data).forEach((element: any) => {
      DB.Ativo.update({
        preco: cmcStocksPrices.data[element].quote.USD.price,
        marketCap: cmcStocksPrices.data[element].quote.USD.market_cap
      },
      {
        where: { id: cmcStocksPrices.data[element].id }
      });
    });
  }

  public async findAllAtivosID (): Promise<Ativo[]> {
    return await DB.Ativo.findAll({ attributes: ['id'] });
  }

  public async findBySymbol (coinSymbol: string): Promise<Ativo[]> {
    return await DB.Ativo.findAll({ where: { codigo: [coinSymbol] } });
  }
}
