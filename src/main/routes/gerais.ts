import { Application } from 'express';
import fs from 'fs';

export default async (app: Application): Promise<void> => {
  app.get('/api', (req: any, res: any) => {
    const appPackage = fs.readFileSync(__dirname + '/../../../package.json').toString();
    const version: string = JSON.parse(appPackage).version;
    res.send('API Version: ' + version);
  });
};
