import { Application } from 'express';
import { adaptRoute } from '@/main/adapters/';
import {
  makeCreateTradeController,
  makeDeleteTradeController,
  makeFindTradeController
} from '@/main/factories/controllers/makeTradesControllers';

export default async (app: Application): Promise<void> => {
  app.post('/api/trades', adaptRoute(makeCreateTradeController()))
    .delete('/api/trades', adaptRoute(makeDeleteTradeController()));

  app.get('/api/trades/getOverview',
    adaptRoute(makeFindTradeController('getOverview')));
  app.get('/api/carteira/getSaldoDolarTrades',
    adaptRoute(makeFindTradeController('getSaldoDolarTrades')));
  app.get('/api/trades/findAll',
    adaptRoute(makeFindTradeController('findAll')));
  app.get('/api/trades/findByAtivo',
    adaptRoute(makeFindTradeController('findByAtivo')));
};
