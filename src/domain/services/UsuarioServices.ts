import { Usuario } from '@/domain/entities/Usuario';
import UsuarioRepository from '@/data/repository/UsuarioRepository';
import GenericServices from './GenericService';
import authTokenGenerator from '@/domain/helpers/authTokenGenerator';
import { TokenPayload } from 'google-auth-library';
import { UsuarioLogin } from '@/domain/types';
import jwt from 'jsonwebtoken';
import { getConfig } from '@/infra/config/config';

export class UsuarioServices extends GenericServices {
  constructor () {
    super(new UsuarioRepository());
  }

  public async updateGoogleCredential (usuario: Usuario): Promise<Usuario> {
    return usuario;
  }

  public async isRegistred (email: string): Promise<Usuario[]> {
    const usuarioRepository = await this.getRepository();
    const usuarios: Usuario[] = await usuarioRepository.findByEmail(email);

    return usuarios;
  }

  public async isAuthorized (authToken: string): Promise<boolean> {
    try {
      const configs = await getConfig();
      const tokenData = jwt.verify(authToken, configs.config.jwtKey);
      const usuarioRepository = await this.getRepository();
      const usuarios: Usuario[] = await usuarioRepository.findByEmailIdAuthToken(tokenData);

      return usuarios.length > 0;
    } catch (error) {
      return false;
    }
  }

  public async insertLogin (usuario: Usuario, ticket: TokenPayload, credential: string, authExpirationTime: number): Promise<UsuarioLogin> {
    const usuarioRepository = await this.getRepository();
    const bearerTokenFinal = authTokenGenerator(ticket);
    const dateNow = new Date();
    const authTokenExpirationDate = new Date();
    authTokenExpirationDate.setMinutes(authTokenExpirationDate.getMinutes() + authExpirationTime);
    const userDataUpdate: Usuario = {
      id: usuario.id,
      nomeCompleto: ticket.name,
      email: ticket.email,
      usuario: ticket.email,
      given_name: ticket.given_name,
      family_name: ticket.given_name,
      credential: credential,
      picture: ticket.picture,
      json_data: JSON.stringify(ticket),
      data_autorizacao: dateNow,
      auth_token: bearerTokenFinal,
      auth_token_expiration: authTokenExpirationDate
    };
    const usuarioLogin: UsuarioLogin = {
      usuario: userDataUpdate,
      update: await usuarioRepository.update(userDataUpdate)
    };

    return usuarioLogin;
  }

  public async logout (authToken: string): Promise<boolean> {
    try {
      const configs = await getConfig();
      const tokenData: any = jwt.verify(authToken, configs.config.jwtKey);
      const usuarioRepository = await this.getRepository();
      const usuario: Usuario = {
        id: tokenData.id,
        auth_token: '',
        email: tokenData.email
      };
      const usuarios: any = await usuarioRepository.update(usuario);

      return usuarios;
    } catch (error) {
      return false;
    }
  }
}
