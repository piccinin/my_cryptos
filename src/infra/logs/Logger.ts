import { ERROR_TYPE } from '@/shared/types';

export default abstract class Logger {
  public static infoLog (msg: any): void {
    console.log(ERROR_TYPE.INFO + ': ', msg);
  }

  public static errorLog (msg: any): void {
    console.log(ERROR_TYPE.ERROR + ': ', msg);
  }
}
