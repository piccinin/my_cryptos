import DB from '@/infra/db/models';
import { Usuario } from '@/domain/entities/Usuario';
import GenericRepository from './GenericRepository';
import { Authorize } from '@/domain/types';

export default class UsuarioRepository extends GenericRepository {
  constructor () {
    super(DB.Usuario, DB.sequelize);
  }

  public async findByEmailIdAuthToken (authorize: Authorize): Promise<Usuario[]> {
    return await DB.Usuario.findAll({
      // logging: console.log,
      where: {
        id: authorize.id,
        email: authorize.email,
        auth_token: authorize.auth_token,
        ativo: 's'
      }
    });
  }

  public async getInvestimentoInicial (userId: number): Promise<number> {
    const usuario: Usuario[] = await DB.Usuario.findAll({
      // logging: console.log,
      where: {
        id: userId
      }
    });

    if (usuario.length > 0) {
      return usuario[0].investimento_inicial;
    } else {
      return 0;
    }
  }

  public async findByEmail (email: string): Promise<Usuario[]> {
    return await DB.Usuario.findAll({
      where: {
        email: email
      }
    });
  }
}
