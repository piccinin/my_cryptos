import { UpdateAtivoController } from "@/presentation/controllers/ativos";
import { AtivoServices } from '@/domain/services'

type SutTypes = {
  sut: UpdateAtivoController
  ativoServicesStub: AtivoServices
}

class AtivoServicesStub extends AtivoServices {
  public async update(entity: any): Promise<any> {
      return false;
  }
}

const makeSut = (): SutTypes => {
  const ativoServicesStub = new AtivoServicesStub();
  const sut = new UpdateAtivoController(ativoServicesStub);
  return {
    sut,
    ativoServicesStub
  }
}

const makeHttpRequestDataMock = (): UpdateAtivoController.Request => ({
    id: 1,
    codigo: 'COD'
})

describe('UpdateAtivoController', () => {
  it('should return HTTP 400 error for missing param id', async () =>{
    const { sut } = makeSut();
    const httpRquest = makeHttpRequestDataMock();
    delete httpRquest.id;
    const httpResponse = await sut.handle(httpRquest);
    expect(httpResponse.statusCode).toBe(400);
  })

  it('should return HTTP 500 if an internal server error happen', async () =>{
    const { sut, ativoServicesStub } = makeSut();
    const httpRquest = makeHttpRequestDataMock();

    jest.spyOn(ativoServicesStub, 'update')
    .mockReturnValue(Promise.resolve(false));

    const httpResponse = await sut.handle(httpRquest);

    expect(httpResponse.statusCode).toBe(500);
  })

  it('should return HTTP 200 if update ativo service was corretly called', async () =>{
    const { sut, ativoServicesStub } = makeSut();
    const httpRquest = makeHttpRequestDataMock();

    jest.spyOn(ativoServicesStub, 'update')
    .mockReturnValueOnce(Promise.resolve(true));

    const httpResponse = await sut.handle(httpRquest);
    expect(httpResponse.statusCode).toBe(200);
  })


})