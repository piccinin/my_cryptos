import { Carteira } from '@/domain/entities/Carteira';
import CarteiraRepository from '@/data/repository/CarteiraRepository';
import UsuarioRepository from '@/data/repository/UsuarioRepository';
import TradeRepository from '@/data/repository/TradeRepository';
import { UpdateSaldoFields } from '@/shared/types';
import GenericServices from './GenericService';

export class CarteiraServices extends GenericServices {
  private readonly carteiraRepository: CarteiraRepository;
  private readonly tradeRepository: TradeRepository;
  private readonly usuarioRepository: UsuarioRepository;

  constructor () {
    super(new CarteiraRepository());
    this.carteiraRepository = new CarteiraRepository();
    this.tradeRepository = new TradeRepository();
    this.usuarioRepository = new UsuarioRepository();
  }

  public async getSaldoTotalEmDolar (): Promise<string> {
    const respTrades = await this.tradeRepository.getSaldoTrades();
    const saldoTrades: number = respTrades[0][0].total;
    const saldoDolar: number = await this.carteiraRepository.getSaldoDolar();
    const saldoTotal = saldoDolar + saldoTrades;
    return JSON.parse(`{ "total": ${saldoTotal} }`);
  }

  public async getSaldoDolarCarteira (): Promise<string> {
    const saldoDolar = await this.carteiraRepository.getSaldoDolar();
    return JSON.parse(`{ "total": ${saldoDolar} }`);
  }

  public async getInvestimentoInicial (codigo: number): Promise<number> {
    const investimentoInicial: number = await this.usuarioRepository.getInvestimentoInicial(codigo);
    return investimentoInicial; // 'JSON.parse(`{ "total": ${saldoDolar} }`)';
  }

  public async updateSaldo (params: UpdateSaldoFields): Promise<Carteira> {
    return await this.carteiraRepository.updateSaldo(params);
  }
}
