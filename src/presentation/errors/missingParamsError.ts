export class MissingParamsError extends Error {
  constructor (paramName: string) {
    super(`Faltando o Parametro ${paramName}`);
    this.name = 'MissingParamsError';
  }
}
