export interface CreateService {
  create: (entity: any) => Promise<boolean>
}
