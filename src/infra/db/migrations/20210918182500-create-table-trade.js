'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.createTable('trades',{
      id: {
        type: Sequelize.DataTypes.BIGINT,
        allowNull: false,
        autoIncrement: true,
        primaryKey: true
      },
      AtivoId: {
        type: Sequelize.DataTypes.BIGINT,
        allowNull: false,
        references: { model : 'ativos', key: 'id'}
      },
      CarteiraId: {
        type: Sequelize.DataTypes.BIGINT,
        allowNull: false,
        references: { model : 'carteiras', key: 'id'}
      },
      data: {
        type: Sequelize.DataTypes.DATE,
        allowNull: false
      },
      tp_trans: {
        type: Sequelize.DataTypes.STRING(1),
        allowNull: false
      },
      qtde: {
        type: Sequelize.DataTypes.DOUBLE,
        allowNull: false
      },
      preco_ativo: {
        type: Sequelize.DataTypes.DOUBLE,
        allowNull: false
      },
      valor: {
        type: Sequelize.DataTypes.DOUBLE,
        allowNull: false
      },
      createdAt: {
        type: Sequelize.DataTypes.DATE,
        allowNull: true
      },
      updatedAt: {
        type: Sequelize.DataTypes.DATE,
        allowNull: true
      }
   });
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.dropTable('trades');
  }
};
