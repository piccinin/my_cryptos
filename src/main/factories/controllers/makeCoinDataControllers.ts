import { Controller } from '@/presentation/protocols/controller';
import { CoinsDataController } from '@/presentation/controllers/coinMarketCap';
import { CoinMarketCapGateway } from '@/infra/gateways/CoinMarketCapGateway';
import { getConfig } from '@/infra/config/config';
import { Parameters } from '@/shared/types';

export const makeCoinDataController = async (): Promise<Controller> => {
  const configs: Parameters = await getConfig();
  return new CoinsDataController(new CoinMarketCapGateway(configs.config.coinMarketCap));
};
