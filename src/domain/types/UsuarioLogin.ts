import { Usuario } from '@/domain/entities';

export type UsuarioLogin = {
  usuario: Usuario
  update: boolean
};
