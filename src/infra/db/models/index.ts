'use strict';

const fs = require('fs');
const path = require('path');
const Sequelize = require('sequelize');
const basename = path.basename(__filename);
const env = 'core';
const config = require('@/infra/config/databases.json')[env];
const db: any = {};

let sequelize: any;
if (config.use_env_variable) {
  sequelize = new Sequelize(process.env[config.use_env_variable], config);
} else {
  sequelize = new Sequelize(config.database, config.username, config.password, config);
}
let modelFolderPath: string = '';
if (process.env.NODE_ENV === 'test') {
  const distDir = __dirname.replace('src', 'dist');
  modelFolderPath = distDir + '/' + env;
} else {
  modelFolderPath = __dirname + '/' + env;
}
fs.readdirSync(modelFolderPath)
  .filter((file: any) => {
    return (file.indexOf('.') !== 0) && (file !== basename) && (file.slice(-3) === '.js');
  })
  .forEach((file: any) => {
    const model = sequelize['import'](path.join(modelFolderPath, file));
    db[model.name] = model;
  });

Object.keys(db).forEach(modelName => {
  if (db[modelName].associate) {
    db[modelName].associate(db);
  }
});

db.sequelize = sequelize;
db.Sequelize = Sequelize;

export default db;
