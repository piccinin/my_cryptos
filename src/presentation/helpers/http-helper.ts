import { ServerError, ForeignKeyError, UnauthorizedError } from '@/presentation/errors';
import { HttpResponse } from '@/presentation/protocols/http';

export const badRequest = (error: Error): HttpResponse => ({
  statusCode: 400,
  body: error
});

export const badRequestDeleteEntity = (error: any): HttpResponse => ({
  statusCode: 400,
  body: new ForeignKeyError(error.stack)
});

export const unauthorizedError = (error: any): HttpResponse => ({
  statusCode: 401,
  body: new UnauthorizedError(error)
});

export const serverError = (error: Error): HttpResponse => ({
  statusCode: 500,
  body: new ServerError(error.stack)
});

export const ok = (data: any): HttpResponse => ({
  statusCode: 200,
  body: data
});
