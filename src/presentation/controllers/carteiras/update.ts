import { Controller, HttpResponse } from '@/presentation/protocols';
import { serverError, badRequest, ok } from '@/presentation/helpers';
import { MissingParamsError } from '@/presentation/errors/';
import { CarteiraServices } from '@/domain/services';

export class UpdateCarteiraController implements Controller {
  constructor (
    readonly carteiraServices: CarteiraServices
  ) {}

  async handle (request: UpdateCarteiraController.Request): Promise<HttpResponse> {
    try {
      for (const field of ['id', 'nome']) {
        if (!Object.prototype.hasOwnProperty.call(request, field)) {
          return badRequest(new MissingParamsError(field));
        }
      }
      const result = await this.carteiraServices.update(request);
      return result ? ok('ok') : serverError(new Error());
    } catch (error: any) {
      console.error(error);
      return serverError(error);
    }
  }
}

export namespace UpdateCarteiraController {
  export type Request = {
    id: number
    codigo?: string
    nome?: string
    preco?: number
    logo?: string
    stopLoss?: number
    alvo?: number
  };
}
