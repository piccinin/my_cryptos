import { Controller, HttpResponse } from '@/presentation/protocols';
import { serverError, badRequest, ok } from '@/presentation/helpers';
import { MissingParamsError } from '@/presentation/errors/';
import { TwitterGateway } from '@/infra/gateways/TwitterGateway';

export class TwitterController implements Controller {
  constructor (
    readonly twitterGateway: TwitterGateway
  ) {}

  async handle (request: TwitterController.Request): Promise<HttpResponse> {
    try {
      for (const field of ['id']) {
        if (!Object.prototype.hasOwnProperty.call(request, field)) {
          return badRequest(new MissingParamsError(field));
        }
      }
      const result = await this.twitterGateway.getTweets(request.id);
      return ok(result);
    } catch (error: any) {
      console.error(error);
      return serverError(error);
    }
  }
}

export namespace TwitterController {
  export type Request = {
    id: number
  };
}
