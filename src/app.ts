import * as bodyParser from 'body-parser';
import express from 'express';
import cors from 'cors';
import rotasAtivos from '@/main/routes/ativos';
import rotasCarteiras from '@/main/routes/carteiras';
import rotasTrades from '@/main/routes/trades';
import rotasGerais from '@/main/routes/gerais';
import rotasAuth from '@/main/routes/auth';
import Logger from '@/infra/logs/Logger';
import authorization from '@/main/middlewares/authorization';

class App {
  public express: express.Application;

  constructor () {
    this.express = express();
    this.middleware();
    void this.routes();
  }

  public async start (): Promise<void> {
    this.express.listen(80, () => {
      Logger.infoLog('Application running on port 80');
    });

    // para criar o banco e rodar as migrations e seeds
    // DB.sequelize.sync();
  }

  // Configure Express middleware.
  private middleware (): void {
    const corsOptions = {
      origin: 'https://mycryptosui.devfpl.com.br',
      credentials: true
    };

    this.express.use(cors(corsOptions));
    this.express.use(bodyParser.json());
    this.express.use(bodyParser.urlencoded({ extended: false }));
    this.express.use(authorization);
  }

  private async routes (): Promise<void> {
    // user route
    await rotasAtivos(this.express);
    await rotasCarteiras(this.express);
    await rotasTrades(this.express);
    await rotasGerais(this.express);
    await rotasAuth(this.express);

    // handle undefined routes
    this.express.use('*', (req, res, next) => {
      const url: string = req.baseUrl;
      const meth: string = req.method;
      res.send('Make sure url is correct!!! ' + url + ' - ' + meth);
    });
  }
}

export default new App();
