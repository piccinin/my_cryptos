import TwitterApi, { TwitterApiReadOnly } from 'twitter-api-v2';
import { Parameters } from '@/shared/types';
import { getConfig } from '@/infra/config/config';
import AtivoRepository from '@/data/repository/AtivoRepository';
import { Ativo } from '@/domain/entities/Ativo';

export class TwitterGateway {
  private client: TwitterApiReadOnly;

  public async connect (): Promise<void> {
    const config: Parameters = await getConfig();
    const twitterClient = new TwitterApi(config.config.twitterConfigs.bearerToken);
    this.client = twitterClient.readOnly;
  }

  public async getTweets (ativoId: number): Promise<any[]> {
    const ativoRepository = new AtivoRepository();
    const ativo: Ativo = await ativoRepository.findById(ativoId);
    const account = ativo.twitter.substring(ativo.twitter.lastIndexOf('/') + 1, ativo.twitter.length);
    await this.connect();
    return await this.readTweets(account);
  }

  public async readTweets (user: string): Promise<any[]> {
    const tweets: any = await this.client.v1.userTimelineByUsername(user, { count: 4 });

    const allTwittes: any = [];
    for (const tweet of tweets) {
      const tData: any = {};
      tData.created_at = tweet.created_at;
      tData.user = user;
      if (tweet.retweeted_status) {
        tData.text = tweet.retweeted_status.full_text;
        const screnName: string = tweet.retweeted_status.user.screen_name;
        const idStr: string = tweet.retweeted_status.id_str;
        tData.url = 'https://twitter.com/' + screnName + '/status/' + idStr;
      } else {
        tData.text = tweet.full_text;
        const idStr: string = tweet.id_str;
        tData.url = `https://twitter.com/${user}/status/${idStr}`;
      }
      allTwittes.push(tData);
    }
    return allTwittes;
  }
}
