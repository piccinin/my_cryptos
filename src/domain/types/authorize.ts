export type Authorize = {
  id: string
  email: string
  auth_token: string
};
