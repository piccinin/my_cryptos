import { CoinsDataController } from "@/presentation/controllers/coinMarketCap";
import { CoinMarketCapGateway } from '@/infra/gateways/CoinMarketCapGateway'
import { Parameters } from "@/shared/types";

type SutTypes = {
  sut: CoinsDataController
  coinMarketCapGateway: CoinMarketCapGateway
}

class CoinMarketCapGatewayStub extends CoinMarketCapGateway {
  public getCoinIdBySymbol(coinSimble: string): Promise<any> {
    return Promise.resolve();
  }
  public getCoinInfoById(id: number): Promise<any> {
    return Promise.resolve();
  }
}

const configsStub: Parameters = {
  config: {
    coinMarketCap: null,
    coinGecko: null,
    twitterConfigs: null
  },
  databases: null
}

const makeSut = (): SutTypes => {
  const coinMarketCapGateway = new CoinMarketCapGatewayStub(configsStub.config.coinMarketCap);
  const sut = new CoinsDataController(coinMarketCapGateway);
  return {
    sut,
    coinMarketCapGateway
  }
}

const makeHttpRequestDataMock = (): CoinsDataController.Request => ({
    codigo: 'any_cod'
})

describe('CoinsDataController', () => {
  it('should return HTTP 400 error for missing param coddigo', async () =>{
    const { sut } = makeSut();
    const httpRquest = makeHttpRequestDataMock();
    delete httpRquest.codigo;
    const httpResponse = await sut.handle(httpRquest);
    expect(httpResponse.statusCode).toBe(400);
  })

  it('should return HTTP 500 if an internal server error happen', async () =>{
    const { sut, coinMarketCapGateway } = makeSut();
    const httpRquest = makeHttpRequestDataMock();

    jest.spyOn(coinMarketCapGateway, 'getCoinIdBySymbol')
    .mockReturnValue(Promise.resolve({}));

    jest.spyOn(coinMarketCapGateway, 'getCoinIdBySymbol')
    .mockReturnValue(Promise.resolve({}));

    const httpResponse = await sut.handle(httpRquest);

    expect(httpResponse.statusCode).toBe(500);
  })

})