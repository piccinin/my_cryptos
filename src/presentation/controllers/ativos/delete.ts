import { Controller, HttpResponse } from '@/presentation/protocols';
import { serverError, badRequest, badRequestDeleteEntity, ok } from '@/presentation/helpers';
import { MissingParamsError } from '@/presentation/errors/';
import { DeleteService } from '@/domain/protocols';

export class DeleteAtivoController implements Controller {
  constructor (
    readonly deleteServvice: DeleteService
  ) {}

  async handle (request: DeleteAtivoController.Request): Promise<HttpResponse> {
    try {
      for (const field of ['id']) {
        if (!Object.prototype.hasOwnProperty.call(request, field)) {
          return badRequest(new MissingParamsError(field));
        }
      }
      const result = await this.deleteServvice.delete(request.id);
      return result ? ok('ok') : serverError(new Error('Internal Error'));
    } catch (error: any) {
      console.error(error.name);
      if (error.name === 'SequelizeForeignKeyConstraintError') {
        return badRequestDeleteEntity(error);
      }
      return serverError(error);
    }
  }
}

export namespace DeleteAtivoController {
  export type Request = {
    id: number
  };
}
