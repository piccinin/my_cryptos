import { DeleteTradeController } from "@/presentation/controllers/trades";
import { TradeServices } from '@/domain/services'

type SutTypes = {
  sut: DeleteTradeController
  tradeServicesStub: TradeServices
}

class TradeServicesStub extends TradeServices {
  public async delete(entity: any): Promise<any> {
      return true;
  }
}

const makeSut = (): SutTypes => {
  const tradeServicesStub = new TradeServicesStub();
  const sut = new DeleteTradeController(tradeServicesStub);
  return {
    sut,
    tradeServicesStub
  }
}

const makeHttpRequestDataMock = (): DeleteTradeController.Request => ({
    id: 1
})

describe('DeleteTradeController', () => {
  it('should return HTTP 400 error for missing param id', async () =>{
    const { sut } = makeSut();
    const httpRquest = makeHttpRequestDataMock();
    delete httpRquest.id;
    const httpResponse = await sut.handle(httpRquest);
    expect(httpResponse.statusCode).toBe(400);
  })

  it('should return HTTP 500 if an internal server error happen', async () =>{
    const { sut, tradeServicesStub } = makeSut();
    const httpRquest = makeHttpRequestDataMock();

    jest.spyOn(tradeServicesStub, 'delete')
    .mockReturnValue(Promise.resolve(false));

    const httpResponse = await sut.handle(httpRquest);
    expect(httpResponse.statusCode).toBe(500);
  })

  it('should return HTTP 200 if delete trade service was corretly called', async () =>{
    const { sut, tradeServicesStub } = makeSut();
    const httpRquest = makeHttpRequestDataMock();

    const httpResponse = await sut.handle(httpRquest);
    expect(httpResponse.statusCode).toBe(200);
  })


})