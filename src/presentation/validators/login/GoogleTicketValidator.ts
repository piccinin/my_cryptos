
import { LoginTicket } from 'google-auth-library';

export default class GoogleTicketValidator {
  public async validate (loginTicket: LoginTicket): Promise<boolean> {
    let valido = false;
    const ticket = loginTicket.getPayload();
    if (ticket) {
      if (ticket.email &&
          ticket.name &&
          ticket.given_name &&
          ticket.email_verified) {
        valido = true;
      }
    }
    return valido;
  }
}
