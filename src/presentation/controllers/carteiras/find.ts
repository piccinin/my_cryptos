import { Controller, HttpResponse } from '@/presentation/protocols';
import { serverError, ok } from '@/presentation/helpers';
import { CarteiraServices } from '@/domain/services';

export class FindCarteiraController implements Controller {
  constructor (
    readonly constructorParams: FindCarteiraController.ContructorParams
  ) {}

  async handle (request: FindCarteiraController.Request): Promise<HttpResponse> {
    try {
      switch (this.constructorParams.operacao) {
        case 'listAll': {
          const result = await this.constructorParams.carteiraServices.findAll();
          return ok(result);
        }
        case 'listAllIdName': {
          const result = await this.constructorParams.carteiraServices.findAllIdName();
          return ok(result);
        }
        case 'getSaldoDolar': {
          const result = await this.constructorParams.carteiraServices.getSaldoTotalEmDolar();
          return ok(result);
        }
        case 'getSaldoDolarCarteira': {
          const result = await this.constructorParams.carteiraServices.getSaldoDolarCarteira();
          return ok(result);
        }
        case 'getInvestimentoInicial': {
          const result = await this.constructorParams.carteiraServices.getInvestimentoInicial(request.userId);
          return ok({ investimentoInicial: result });
        }
        default: { throw new Error('Method not found'); }
      }
    } catch (error: any) {
      console.error(error);
      return serverError(error);
    }
  }
}

export namespace FindCarteiraController {
  export type Request = {
    userId: number
  };

  export type ContructorParams = {
    readonly carteiraServices: CarteiraServices
    readonly operacao: string
  };
}
