export class ForeignKeyError extends Error {
  constructor (stack: any) {
    super('ForeignKey');
    this.name = 'ForeignKeyError';
    this.stack = stack;
  }
}
