export interface DeleteService {
  delete: (id: number) => Promise<any>
}
