'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    return Promise.all([
      queryInterface.addColumn(
        'usuarios',
        'picture',
        {
          type: Sequelize.DataTypes.STRING,
          allowNull: true
        }
      ),
      queryInterface.addColumn(
        'usuarios',
        'given_name',
        {
          type: Sequelize.DataTypes.STRING,
          allowNull: true
        }
      ),
      queryInterface.addColumn(
        'usuarios',
        'family_name',
        {
          type: Sequelize.DataTypes.STRING,
          allowNull: true
        }
      ),
      queryInterface.addColumn(
        'usuarios',
        'credential',
        {
          type: Sequelize.DataTypes.STRING,
          allowNull: true
        }
      ),
      queryInterface.addColumn(
        'usuarios',
        'json_data',
        {
          type: Sequelize.DataTypes.DOUBLE,
          allowNull: true
        }
      ),
      queryInterface.addColumn(
        'usuarios',
        'ativo',
        {
          type: Sequelize.DataTypes.STRING(1),
          allowNull: true
        }
      ),
      queryInterface.addColumn(
        'usuarios',
        'auth_token',
        {
          type: Sequelize.DataTypes.STRING,
          allowNull: true
        }
      ),
    ]);
  },

  down: (queryInterface, Sequelize) => {
  }
};
