'use strict';

import { Model } from 'sequelize';

interface UsuarioAttributes {
  id: number
  nomeCompleto: string
  email: string
  usuario?: string
  senha?: string
  investimento_inicial?: number
  createdAt: Date
  updatedAt: Date
  picture?: string
  given_name?: string
  family_name?: string
  credential?: string
  json_data?: string
  ativo?: string
  auth_token?: string
  auth_token_expiration?: Date
  data_ativacao?: Date
  data_desativacao?: Date
  data_autorizacao?: Date
}

// eslint-disable-next-line @typescript-eslint/explicit-function-return-type
export default (sequelize: any, DataTypes: any) => {
  class Usuario extends Model<UsuarioAttributes> implements UsuarioAttributes {
    id: number;
    nomeCompleto: string;
    email: string;
    usuario?: string;
    senha?: string;
    investimento_inicial?: number;
    createdAt: Date;
    updatedAt: Date;
    picture?: string;
    given_name?: string;
    family_name?: string;
    credential?: string;
    json_data?: string;
    ativo?: string;
    auth_token?: string;
    auth_token_expiration?: Date;
    data_ativacao?: Date;
    data_desativacao?: Date;
    data_autorizacao?: Date;
    static associate (models: any): void {
    }
  }

  Usuario.init({
    id: {
      type: DataTypes.BIGINT,
      allowNull: false,
      autoIncrement: true,
      primaryKey: true
    },
    nomeCompleto: {
      type: DataTypes.STRING,
      allowNull: false
    },
    email: {
      type: DataTypes.STRING,
      allowNull: false
    },
    usuario: {
      type: DataTypes.STRING,
      allowNull: true
    },
    senha: {
      type: DataTypes.STRING,
      allowNull: true
    },
    investimento_inicial: {
      type: DataTypes.DOUBLE,
      allowNull: true
    },
    createdAt: {
      type: DataTypes.DATE,
      allowNull: true
    },
    updatedAt: {
      type: DataTypes.DATE,
      allowNull: true
    },
    picture: {
      type: DataTypes.STRING,
      allowNull: true
    },
    given_name: {
      type: DataTypes.STRING,
      allowNull: true
    },
    family_name: {
      type: DataTypes.STRING,
      allowNull: true
    },
    credential: {
      type: DataTypes.STRING,
      allowNull: true
    },
    json_data: {
      type: DataTypes.STRING,
      allowNull: true
    },
    ativo: {
      type: DataTypes.STRING,
      allowNull: true
    },
    auth_token: {
      type: DataTypes.STRING,
      allowNull: true
    },
    auth_token_expiration: {
      type: DataTypes.DATE,
      allowNull: true
    },
    data_ativacao: {
      type: DataTypes.DATE,
      allowNull: true
    },
    data_desativacao: {
      type: DataTypes.DATE,
      allowNull: true
    },
    data_autorizacao: {
      type: DataTypes.DATE,
      allowNull: true
    }
  }, {
    sequelize,
    tableName: 'usuarios',
    modelName: 'Usuario',
    updatedAt: false,
    createdAt: false
  });

  return Usuario;
};
