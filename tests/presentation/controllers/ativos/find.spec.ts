import { FindAtivoController } from "@/presentation/controllers/ativos";
import { AtivoServices } from '@/domain/services'

type SutTypes = {
  sut: FindAtivoController
  ativoServicesStub: AtivoServices
  constructorParams: FindAtivoController.ContructorParams
}

class AtivoServicesStub extends AtivoServices {
  public async delete(entity: any): Promise<any> {
      return true;
  }
}

const makeSut = (): SutTypes => {
  const ativoServicesStub = new AtivoServicesStub();
  const constructorParams : FindAtivoController.ContructorParams = {
    findAllIdNameService: ativoServicesStub,
    findAllService: ativoServicesStub,
    operacao: ''
  }
  const sut = new FindAtivoController(constructorParams);
  return {
    sut,
    ativoServicesStub,
    constructorParams
  }
}

describe('FindAtivoController', () => {
  // it('should return HTTP 400 error for missing param id', async () =>{
  //   const { sut } = makeSut();
  //   const httpRquest = makeHttpRequestDataMock();
  //   delete httpRquest.id;
  //   const httpResponse = await sut.handle(httpRquest);
  //   expect(httpResponse.statusCode).toBe(400);
  // })

  // it('should return HTTP 500 if an internal server error happen', async () =>{
  //   const { sut, ativoServicesStub } = makeSut();
  //   const httpRquest = makeHttpRequestDataMock();

  //   jest.spyOn(ativoServicesStub, 'delete')
  //   .mockReturnValue(Promise.resolve(false));

  //   const httpResponse = await sut.handle(httpRquest);
  //   expect(httpResponse.statusCode).toBe(500);
  // })

  // it('should return HTTP 200 if delete ativo service was corretly called', async () =>{
  //   const { sut, ativoServicesStub } = makeSut();
  //   const httpRquest = makeHttpRequestDataMock();

  //   const httpResponse = await sut.handle(httpRquest);
  //   expect(httpResponse.statusCode).toBe(200);
  // })
  it('', async () => {
    expect(1).toBe(1);
  });

})