import { createHash } from 'crypto';
import { TokenPayload } from 'google-auth-library';

export default function (jsonData: TokenPayload): string {
  const HASH_PHRASE: string = 'batatinha quando nasce...';
  const DATA_HORA_UNIX: string = String(Math.floor(Date.now() / 1000));
  const JSON_DATA = JSON.stringify(jsonData);
  const hash1Raw = createHash('sha256').update(HASH_PHRASE + DATA_HORA_UNIX + JSON_DATA).digest('base64');
  const hash2Buff = Buffer.from(hash1Raw, 'base64');
  const bearerTokenFinal = createHash('sha256').update(hash2Buff).digest('base64');

  return bearerTokenFinal;
}
