import DB from '@/infra/db/models';
import { Carteira } from '@/domain/entities/Carteira';
import GenericRepository from '@/data/repository/GenericRepository';
import { UpdateSaldoFields } from '@/shared/types';

export default class CarteiraRepository extends GenericRepository {
  constructor () {
    super(DB.Carteira, DB.sequelize);
  }

  public async getSaldoDolar (): Promise<number> {
    return await DB.Carteira.sum('saldoDolar');
  }

  public async updateSaldo (params: UpdateSaldoFields): Promise<Carteira> {
    const carteira: Carteira = await this.findById(params.id);
    carteira.saldoDolar = carteira.saldoDolar + (params.op * params.valor);
    return await DB.Carteira.update({ saldoDolar: carteira.saldoDolar }, { where: { id: carteira.id } });
  }
}
