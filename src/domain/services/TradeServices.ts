import { UpdateSaldoFields } from '@/shared/types';
import { CarteiraServices } from '@/domain/services';
import GenericServices from './GenericService';
import TradeRepository from '@/data/repository/TradeRepository';

export class TradeServices extends GenericServices {
  private readonly tradeRepository: TradeRepository;

  constructor () {
    super(new TradeRepository());
    this.tradeRepository = new TradeRepository();
  }

  public async create (trade: any): Promise<boolean> {
    try {
      trade.data = new Date();
      trade.valor = trade.tp_trans === 'b' ? trade.valor : trade.valor * -1;
      trade.qtde = trade.tp_trans === 'b' ? trade.qtde : trade.qtde * -1;
      const tradeResult = await this.tradeRepository.create(trade);
      const op = -1;
      const params: UpdateSaldoFields = {
        id: tradeResult.dataValues.CarteiraId,
        valor: tradeResult.dataValues.valor,
        op: op
      };
      const carteiraServices = new CarteiraServices();
      try {
        const carteiraResult = await carteiraServices.updateSaldo(params);
        return typeof carteiraResult === 'object';
      } catch (error: any) {
        void this.tradeRepository.delete(tradeResult.dataValues.id);
        return false;
      }
    } catch (error: any) {
      return false;
    }
  }

  public async delete (tradeId: number): Promise<boolean> {
    try {
      const tradeToDelete = await this.tradeRepository.findById(tradeId);
      // atualizar o saldo da carteira antes de remover o trade
      const op = 1;
      const params: UpdateSaldoFields = {
        id: tradeToDelete.dataValues.CarteiraId,
        valor: tradeToDelete.dataValues.valor,
        op: op
      };
      const carteiraServices = new CarteiraServices();
      const updatedSadoResult = await carteiraServices.updateSaldo(params);
      if (typeof updatedSadoResult === 'object') {
        if (await this.tradeRepository.delete(tradeId)) {
          return true;
        } else {
          return false;
        }
      }
    } catch (error: any) {
      return false;
    }
  }

  public async getOverview (): Promise<any> {
    return await this.tradeRepository.getOverview();
  }

  public async getSaldoDolarTrades (): Promise<number> {
    const respTrades = await this.tradeRepository.getSaldoTrades();
    const saldoTrades: number = respTrades[0][0].total;
    return JSON.parse(`{ "total": ${saldoTrades} }`);
  }

  public async findByAtivoId (aitivoId: Number): Promise<any[]> {
    return await this.tradeRepository.findByAtivoId(aitivoId);
  }
}
