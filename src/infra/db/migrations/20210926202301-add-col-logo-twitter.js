'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    return Promise.all([
      queryInterface.addColumn(
        'ativos',
        'logo',
        {
          type: Sequelize.DataTypes.STRING,
          allowNull: true
        }
      ),
      queryInterface.addColumn(
        'ativos',
        'twitter',
        {
          type: Sequelize.DataTypes.STRING,
          allowNull: true
        }
      ),
    ]);
  },

  down: (queryInterface, Sequelize) => {
  }
};
