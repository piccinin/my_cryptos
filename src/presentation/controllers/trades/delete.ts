import { Controller, HttpResponse } from '@/presentation/protocols';
import { serverError, badRequest, ok } from '@/presentation/helpers';
import { MissingParamsError } from '@/presentation/errors/';
import { TradeServices } from '@/domain/services';

export class DeleteTradeController implements Controller {
  constructor (
    readonly tradeServices: TradeServices
  ) {}

  async handle (request: DeleteTradeController.Request): Promise<HttpResponse> {
    try {
      const fieldsToValidate = ['id'];
      for (const field of fieldsToValidate) {
        if (!Object.prototype.hasOwnProperty.call(request, field)) {
          return badRequest(new MissingParamsError(field));
        }
      }
      const result = await this.tradeServices.delete(request.id);
      return result ? ok('ok') : serverError(new Error());
    } catch (error: any) {
      console.error(error);
      return serverError(error);
    }
  }
}

export namespace DeleteTradeController {
  export type Request = {
    id: number
  };
}
