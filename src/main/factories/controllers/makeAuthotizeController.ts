import { Controller } from '@/presentation/protocols/controller';
import { AuthorizeController } from '@/presentation/controllers/auth';
import { UsuarioServices } from '@/domain/services';

export const makeAuthorizeController = (): Controller => {
  return new AuthorizeController(new UsuarioServices());
};
