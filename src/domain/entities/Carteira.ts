export interface Carteira {
  id: number
  nome: string
  saldoDolar: number
  descricao?: string
  createdAt?: Date
  updatedAt?: Date
}
