import { Controller, HttpResponse } from '@/presentation/protocols';
import { serverError, badRequest, ok } from '@/presentation/helpers';
import { MissingParamsError } from '@/presentation/errors/';
import { TradeServices } from '@/domain/services';

export class CreateTradeController implements Controller {
  constructor (
    readonly tradeServices: TradeServices
  ) {}

  async handle (request: CreateTradeController.Request): Promise<HttpResponse> {
    try {
      const fieldsToValidate = [
        'AtivoId', 'CarteiraId', 'tp_trans', 'qtde', 'preco_ativo', 'valor'
      ];
      for (const field of fieldsToValidate) {
        if (!Object.prototype.hasOwnProperty.call(request, field)) {
          return badRequest(new MissingParamsError(field));
        }
      }
      const result = await this.tradeServices.create(request);
      return result ? ok('ok') : serverError(new Error());
    } catch (error: any) {
      console.error(error);
      return serverError(error);
    }
  }
}

export namespace CreateTradeController {
  export type Request = {
    AtivoId: number
    CarteiraId: number
    tp_trans: string
    qtde: number
    preco_ativo: number
    valor: number
  };
}
