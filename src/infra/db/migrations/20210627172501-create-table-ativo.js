'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.createTable('ativos',{
      id: {
        type: Sequelize.DataTypes.BIGINT,
        allowNull: false,
        autoIncrement: true,
        primaryKey: true
      },
      codigo: {
        type: Sequelize.DataTypes.STRING(5),
        allowNull: false
      },
      nome: {
        type: Sequelize.DataTypes.STRING(50),
        allowNull: false
      },
      preco: {
        type: Sequelize.DataTypes.DOUBLE,
        allowNull: false
      },
      descricao: {
        type: Sequelize.DataTypes.STRING,
        allowNull: true
      },
      stopLoss: {
        type: Sequelize.DataTypes.DOUBLE,
        allowNull: true
      },
      alvo: {
        type: Sequelize.DataTypes.DOUBLE,
        allowNull: true
      },
      createdAt: {
        type: Sequelize.DataTypes.DATE,
        allowNull: true
      },
      updatedAt: {
        type: Sequelize.DataTypes.DATE,
        allowNull: true
      }
   });
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.dropTable('ativos');
  }
};
