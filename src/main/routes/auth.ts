import { Application } from 'express';
import { adaptRoute } from '../adapters';
import { makeLoginController } from '@/main/factories/controllers/makeLoginControllers';
import { makeLogoutController } from '@/main/factories/controllers/makeLogoutController';
import { makeAuthorizeController } from '@/main/factories/controllers/makeAuthotizeController';

export default async (app: Application): Promise<void> => {
  app.post('/login', adaptRoute(makeLoginController()));
  app.patch('/logout', adaptRoute(makeLogoutController()));
  app.get('/authorize', adaptRoute(makeAuthorizeController()));
};
