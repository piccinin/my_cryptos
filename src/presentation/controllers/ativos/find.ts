import { Controller, HttpResponse } from '@/presentation/protocols';
import { serverError, ok } from '@/presentation/helpers';
import { FindAllService, FindAllIdNameService } from '@/domain/protocols';

export class FindAtivoController implements Controller {
  constructor (
    readonly constructorParams: FindAtivoController.ContructorParams
  ) {}

  async handle (request: FindAtivoController.Request): Promise<HttpResponse> {
    try {
      switch (this.constructorParams.operacao) {
        case 'listAll': {
          const result = await this.constructorParams.findAllService.findAll();
          return ok(result);
        }
        case 'listAllIdName': {
          const result = await this.constructorParams.findAllIdNameService.findAllIdName();
          return ok(result);
        }
        default: { throw new Error('Method not found'); }
      }
    } catch (error: any) {
      console.error(error);
      return serverError(error);
    }
  }
}

export namespace FindAtivoController {
  export type Request = {
    codigo: string
  };

  export type ContructorParams = {
    readonly findAllService: FindAllService
    readonly findAllIdNameService: FindAllIdNameService
    readonly operacao: string
  };
}
