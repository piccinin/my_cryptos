import { Ativo } from '@/domain/entities';
import { AtivoSaveLote, AtivoPriceUpdate, AtivoFindAllAtivosID, AtivoFindBySymbol, AtivoUpdateQuotes } from '@/domain/protocols/ativos';
import AtivoRepository from '@/data/repository/AtivoRepository';
import HttpPoller from '@/infra/pooler/http-poller';
import { getConfig } from '@/infra/config/config';
import GenericServices from './GenericService';

export class AtivoServices extends GenericServices implements
 AtivoSaveLote,
 AtivoPriceUpdate,
 AtivoFindAllAtivosID,
 AtivoFindBySymbol,
 AtivoUpdateQuotes {
  constructor () {
    super(new AtivoRepository());
  }

  public async saveLote (cmcStocksTwitter: any): Promise<void> {
    const ativoRepository = await this.getRepository();
    await ativoRepository.saveLote(cmcStocksTwitter);
  }

  public async priceUpdate (cmcStocksPrices: any): Promise<void> {
    void this.getRepository().then(ativoRespository => {
      ativoRespository.priceUpdate(cmcStocksPrices);
    });
  }

  public async findAllAtivosID (): Promise<Ativo[]> {
    const ativoRepository = await this.getRepository();
    return await ativoRepository.findAllAtivosID();
  }

  public async findBySymbol (coinSymbol: string): Promise<Ativo[]> {
    const ativoRepository = await this.getRepository();
    return await ativoRepository.findBySymbol(coinSymbol);
  }

  public async updateQuotes (): Promise<boolean> {
    const configs = await getConfig();
    const httpPoller = new HttpPoller(configs.config);
    const result = await httpPoller.updateQuotes();
    return result === 'ok';
  }
}
