import { CreateAtivoController } from "@/presentation/controllers/ativos";
import { AtivoServices } from '@/domain/services'

type SutTypes = {
  sut: CreateAtivoController
  ativoServicesStub: AtivoServices
}

class AtivoServicesStub extends AtivoServices {
  public async create(entity: any): Promise<any> {
      return false;
  }
}

const makeSut = (): SutTypes => {
  const ativoServicesStub = new AtivoServicesStub();
  const sut = new CreateAtivoController(ativoServicesStub);
  return {
    sut,
    ativoServicesStub
  }
}

const makeHttpRequestDataMock = (): CreateAtivoController.Request => ({
    codigo: 'COD',
    nome: 'any_name',
    preco: 0,
    logo: 'any_log'
})

describe('CreateAtivoController', () => {
  it('should return HTTP 400 error for missing param codigo', async () =>{
    const { sut } = makeSut();
    const httpRquest = makeHttpRequestDataMock();
    delete httpRquest.codigo;
    const httpResponse = await sut.handle(httpRquest);
    expect(httpResponse.statusCode).toBe(400);
  })

  it('should return HTTP 400 error for missing param nome', async () =>{
    const { sut } = makeSut();
    const httpRquest = makeHttpRequestDataMock();
    delete httpRquest.nome;
    const httpResponse = await sut.handle(httpRquest);
    expect(httpResponse.statusCode).toBe(400);
  })

  it('should return HTTP 400 error for missing param preco', async () =>{
    const { sut } = makeSut();
    const httpRquest = makeHttpRequestDataMock();
    delete httpRquest.preco;
    const httpResponse = await sut.handle(httpRquest);
    expect(httpResponse.statusCode).toBe(400);
  })

  it('should return HTTP 400 error for missing param logo', async () =>{
    const { sut } = makeSut();
    const httpRquest = makeHttpRequestDataMock();
    delete httpRquest.logo;
    const httpResponse = await sut.handle(httpRquest);
    expect(httpResponse.statusCode).toBe(400);
  })

  it('should return HTTP 500 error if codigo length is greater the 5', async () =>{
    const { sut } = makeSut();
    const httpRquest = makeHttpRequestDataMock();
    httpRquest.codigo = 'COD_Long';
    const httpResponse = await sut.handle(httpRquest);
    expect(httpResponse.statusCode).toBe(500);
  })

  it('should return HTTP 500 if an internal server error happen', async () =>{
    const { sut, ativoServicesStub } = makeSut();
    const httpRquest = makeHttpRequestDataMock();

    jest.spyOn(ativoServicesStub, 'create')
    .mockReturnValue(Promise.reject(new Error()));

    const httpResponse = await sut.handle(httpRquest);

    expect(httpResponse.statusCode).toBe(500);
  })

  it('should return HTTP 200 if create ativo service was corretly called', async () =>{
    const { sut, ativoServicesStub } = makeSut();
    const httpRquest = makeHttpRequestDataMock();

    jest.spyOn(ativoServicesStub, 'create')
    .mockReturnValueOnce(Promise.resolve(true));

    const httpResponse = await sut.handle(httpRquest);
    expect(httpResponse.statusCode).toBe(200);
  })


})