export interface FindAllService {
  findAll: () => Promise<any[]>
}
