import { Controller, HttpResponse } from '@/presentation/protocols';
import { LoginServices } from '@/domain/services';
import { serverError, unauthorizedError, ok } from '@/presentation/helpers';
import GoogleAuth from '@/presentation/helpers/GoogleAuth';

export class LoginController implements Controller {
  constructor (
    readonly loginServices: LoginServices
  ) {}

  async handle (request: LoginController.Request): Promise<HttpResponse> {
    try {
      const googleAuth = new GoogleAuth();
      const credential = request.credential;
      const loginTicket: any = await googleAuth.verifyGoogleUser(credential);
      if (loginTicket) {
        const usuario: string = await this.loginServices.login(loginTicket.getPayload(), credential);
        if (usuario.length < 1) {
          return unauthorizedError('Usuário não cadastrado');
        }
        return ok(usuario);
      } else {
        return unauthorizedError('Credencial Invállida');
      }
    } catch (error: any) {
      return serverError(error);
    }
  }
}

export namespace LoginController {
  export type Request = {
    credential: string
  };
}
