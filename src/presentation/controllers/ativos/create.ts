import { Controller, HttpResponse } from '@/presentation/protocols';
import { serverError, badRequest, ok } from '@/presentation/helpers';
import { MissingParamsError } from '@/presentation/errors/';
import { CreateService } from '@/domain/protocols';

export class CreateAtivoController implements Controller {
  constructor (
    readonly ativoServices: CreateService
  ) {}

  async handle (request: CreateAtivoController.Request): Promise<HttpResponse> {
    try {
      for (const field of ['codigo', 'nome', 'preco', 'logo']) {
        if (!Object.prototype.hasOwnProperty.call(request, field)) {
          return badRequest(new MissingParamsError(field));
        }
      }
      const result = await this.ativoServices.create(request);
      return result ? ok('ok') : serverError(new Error());
    } catch (error: any) {
      return serverError(error);
    }
  }
}

export namespace CreateAtivoController {
  export type Request = {
    codigo: string
    nome: string
    preco: number
    logo: string
    stopLoss?: number
    twitter?: number
    alvo?: number
  };
}
