import {
  CreateService,
  DeleteService,
  UpdateService,
  FindAllService,
  FindAllIdNameService,
  FindByIdService
} from '@/domain/protocols';

export default class GenericServices
implements CreateService,
  DeleteService,
  UpdateService,
  FindAllService,
  FindAllIdNameService,
  FindByIdService {
  private readonly repository: any;

  constructor (repository: any) {
    this.repository = repository;
  }

  public async getRepository (): Promise<any> {
    return this.repository;
  }

  public async create (entity: any): Promise<boolean> {
    const created = await this.repository.create(entity);
    return typeof created === 'object';
  }

  public async delete (id: number): Promise<any> {
    const deleted = await this.repository.delete(id);
    return deleted;
  }

  public async update (entity: any): Promise<boolean> {
    const updated = await this.repository.update(entity);
    return typeof updated === 'object';
  }

  public async findAll (): Promise<any[]> {
    return await this.repository.findAll();
  }

  public async findAllIdName (): Promise<any[]> {
    return await this.repository.findAllIdName();
  }

  public async findById (id: number): Promise<any> {
    return await this.repository.findById(id);
  }
}
