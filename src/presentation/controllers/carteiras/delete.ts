import { Controller, HttpResponse } from '@/presentation/protocols';
import { serverError, badRequest, badRequestDeleteEntity, ok } from '@/presentation/helpers';
import { MissingParamsError } from '@/presentation/errors/';
import { CarteiraServices } from '@/domain/services';

export class DeleteCarteiraController implements Controller {
  constructor (
    readonly carteiraServices: CarteiraServices
  ) {}

  async handle (request: DeleteCarteiraController.Request): Promise<HttpResponse> {
    try {
      for (const field of ['id']) {
        if (!Object.prototype.hasOwnProperty.call(request, field)) {
          return badRequest(new MissingParamsError(field));
        }
      }
      const result = await this.carteiraServices.delete(request.id);
      return result ? ok('ok') : serverError(new Error('Internal Error'));
    } catch (error: any) {
      console.error(error.name);
      if (error.name === 'SequelizeForeignKeyConstraintError') {
        return badRequestDeleteEntity(error);
      }
      return serverError(error);
    }
  }
}

export namespace DeleteCarteiraController {
  export type Request = {
    id: number
  };
}
