import { OAuth2Client, LoginTicket } from 'google-auth-library';
import GoogleTicketValidator from '@/presentation/validators/login/GoogleTicketValidator';

export default class GoogleAuth {
  public async verifyGoogleUser (credential: string): Promise<boolean | LoginTicket> {
    const CLIENT_ID = '747002520458-h4234tjq3mqf5gf887lhernom8q7j3bu.apps.googleusercontent.com';
    const CLIENT_SECRET = 'OCSPX-c23hz4I5XDLk1GDwo4GbnsqI1cjq';
    const redirectURL = 'https://mycryptosui.devfpl.com.br';
    const oAuth2Client = new OAuth2Client(
      CLIENT_ID,
      CLIENT_SECRET,
      redirectURL
    );

    try {
      const ticket: LoginTicket = await oAuth2Client.verifyIdToken({ idToken: credential, audience: CLIENT_ID });
      const googleTicketValidator = new GoogleTicketValidator();

      if (await googleTicketValidator.validate(ticket)) {
        return ticket;
      } else {
        return false;
      }
    } catch (error: any) {
      return false;
    }
  }
}
