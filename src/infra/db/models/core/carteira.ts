'use strict';

import { Model } from 'sequelize';

interface CarteiraAttributes {
  id: number
  nome: string
  saldoDolar: number
  descricao: string
  createdAt: Date
  updatedAt: Date
}

// eslint-disable-next-line @typescript-eslint/explicit-function-return-type
export default (sequelize: any, DataTypes: any) => {
  class Carteira extends Model<CarteiraAttributes> implements CarteiraAttributes {
    id: number;
    nome: string;
    saldoDolar: number;
    descricao: string;
    createdAt: Date;
    updatedAt: Date;
    static associate (models: any): void {
      Carteira.hasMany(models.Trade);
    }
  }

  Carteira.init({
    id: {
      type: DataTypes.BIGINT,
      allowNull: false,
      autoIncrement: true,
      primaryKey: true
    },
    nome: {
      type: DataTypes.STRING(50),
      allowNull: false
    },
    saldoDolar: {
      type: DataTypes.DOUBLE,
      allowNull: true
    },
    descricao: {
      type: DataTypes.STRING,
      allowNull: true
    },
    createdAt: {
      type: DataTypes.DATE,
      allowNull: true
    },
    updatedAt: {
      type: DataTypes.DATE,
      allowNull: true
    }
  }, {
    sequelize,
    tableName: 'carteiras',
    modelName: 'Carteira'
  });

  return Carteira;
};
