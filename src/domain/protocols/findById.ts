export interface FindByIdService {
  findById: (id: number) => Promise<any>
}
