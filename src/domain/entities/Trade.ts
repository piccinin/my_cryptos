export interface Trade {
  id: number
  AtivoId: number
  CarteiraId: number
  tp_trans: string
  qtde: number
  preco_ativo: number
  valor: number
  data: Date
  createdAt: Date
  updatedAt: Date
}
