import { ENV, Parameters } from '@/shared/types';
import * as fs from 'fs';
import * as readPkgUp from 'read-pkg-up';
const env = process.env.NODE_ENV || ENV.DEVELOPMENT;

const buildParamName = (param: string): string => {
  const appPackage = readPkgUp.sync({ normalize: false }).packageJson;
  return `/${env}/${appPackage.name}/${param}`;
};

const PARAMETER_CONFIG = buildParamName('config');
const PARAMETER_DATABASES = buildParamName('databases');

const getParameters = async (): Promise<{ [key: string]: string }> => {
  if (env === ENV.DEVELOPMENT) {
    return {
      [PARAMETER_CONFIG]: fs.readFileSync(`${__dirname}/configs.json`).toString(),
      [PARAMETER_DATABASES]: fs.readFileSync(`${__dirname}/databases.json`).toString()
    };
  } else {
    return {
      [PARAMETER_CONFIG]: fs.readFileSync(`${__dirname}/configs.json`).toString(),
      [PARAMETER_DATABASES]: fs.readFileSync(`${__dirname}/databases.json`).toString()
    };
  }
};

export const getConfig = async (): Promise<Parameters> => {
  const parameters = await getParameters();
  const config: Parameters = {
    config: JSON.parse(parameters[PARAMETER_CONFIG]),
    databases: JSON.parse(parameters[PARAMETER_DATABASES])
  };
  return config;
};
