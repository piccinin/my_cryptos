import DB from '@/infra/db/models';
import { Trade } from '@/domain/entities/Trade';
import GenericRepository from '@/data/repository/GenericRepository';
import { QueryTypes } from 'sequelize';

export default class TradeRepository extends GenericRepository {
  constructor () {
    super(DB.Trade, DB.sequelize);
  }

  public async findAll (): Promise<Trade[]> {
    return await DB.sequelize.query(
      `SELECT
        t.id,
        t.\`data\`,
        a.codigo,
        a.nome as ativo,
        a.logo,
        c.nome as carteira,
        t.tp_trans,
        t.qtde,
        t.preco_ativo,
        t.valor
      FROM
        trades t,
        ativos a,
        carteiras c
      WHERE
        a.id = t.AtivoId AND
        c.id = t.CarteiraId
      ORDER BY t.\`data\` DESC
    `);
  }

  public async findByAtivoId (aitivoId: Number): Promise<any[]> {
    return await DB.sequelize.query(
      `SELECT
        t.id,
        t.\`data\`,
        a.codigo,
        a.nome as ativo,
        a.logo,
        c.nome as carteira,
        t.tp_trans,
        t.qtde,
        t.preco_ativo,
        t.valor
      FROM
        trades t,
        ativos a,
        carteiras c
      WHERE
        a.id = ? AND
        a.id = t.AtivoId AND
        c.id = t.CarteiraId
      ORDER BY t.\`data\` DESC
    `, {
        replacements: [aitivoId],
        type: QueryTypes.SELECT
      });
  }

  public async getOverview (): Promise<any> {
    return await DB.sequelize.query(`
    SELECT
      dadosOV.AtivoId,
      dadosOV.preco,
      dadosOV.codigo,
      dadosOV.nome,
      dadosOV.logo,
      dadosOV.stopLoss,
      dadosOV.alvo,
      dadosOV.marketCap,
      dadosOV.qtde,
      dadosOV.valor_investido,
      dadosOV.valor_investido_compras,
      dadosOV.valor_medio_compra,
      dadosOV.valor_atual,
      t.preco_ativo as ultima_negociacao,
      t.tp_trans
    FROM (
      SELECT MAX(t.id) as tradeID, saldo_ativos.* FROM (
          SELECT
            t.AtivoId,
              a.preco,
              a.codigo as codigo,
              a.nome as nome,
              a.logo as logo,
              a.stopLoss as stopLoss,
              a.alvo as alvo,
              a.marketCap,
              SUM(t.qtde) as qtde,
              SUM( CASE WHEN t.tp_trans='b' THEN t.valor ELSE 0 END ) as valor_investido_compras,
              SUM(t.valor) as valor_investido,
              SUM( CASE WHEN t.tp_trans='b' THEN t.valor ELSE 0 END ) / SUM( CASE WHEN t.tp_trans='b' THEN t.qtde ELSE 0 END ) as valor_medio_compra,
              SUM(t.qtde) * a.preco as valor_atual
          FROM
            trades t, ativos a
          WHERE
            t.AtivoId = a.id
          group by t.AtivoId, a.nome, a.codigo, a.preco
        ) as saldo_ativos, trades as t
        WHERE
            saldo_ativos.valor_medio_compra IS NOT NULL
        AND saldo_ativos.AtivoId = t.AtivoId
        AND saldo_ativos.valor_atual > 0
        GROUP BY saldo_ativos.AtivoId,
              saldo_ativos.preco,
              saldo_ativos.codigo,
              saldo_ativos.nome,
              saldo_ativos.logo,
              saldo_ativos.stopLoss,
              saldo_ativos.alvo,
              saldo_ativos.marketCap,
              saldo_ativos.qtde,
              saldo_ativos.valor_investido,
              saldo_ativos.valor_medio_compra,

              saldo_ativos.valor_atual
    ) as dadosOV, trades as t
    WHERE dadosOV.tradeID = t.id
  `);
  }

  public async getSaldoTrades (): Promise<any[]> {
    return await DB.sequelize.query(`
    SELECT SUM(valor_atual) as total FROM (
      SELECT
        t.AtivoId,
          SUM(t.qtde) * a.preco as valor_atual
      FROM
        trades t, ativos a
      WHERE
        t.AtivoId = a.id
      group by t.AtivoId, a.nome, a.codigo, a.preco
    ) as saldo_ativos
    `);
  }

  public async getTradesGroupByCarteira (): Promise<any[]> {
    return await DB.sequelize.query(`
      SELECT
        saldo_ativos.CarteiraId,
        saldo_ativos.AtivoId,
        c.nome as carteira,
        a.nome as artivo,
        saldo_ativos.valor_atual
      FROM (
        SELECT
          t.CarteiraId,
          t.AtivoId,
            SUM(t.qtde) * a.preco as valor_atual
        FROM
          trades t, ativos a
        WHERE
          t.AtivoId = a.id
        group by t.CarteiraId, t.AtivoId
      ) as saldo_ativos, ativos a, carteiras c
      where saldo_ativos.AtivoId = a.id
        AND saldo_ativos.CarteiraId = c.id
        AND saldo_ativos.valor_atual > 0
      order by
        saldo_ativos.CarteiraId,
      saldo_ativos.valor_atual DESC
    `);
  }
}
