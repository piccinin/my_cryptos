'use strict';

import { Model } from 'sequelize';

interface TradeAttributes {
  id: number
  AtivoId: number
  CarteiraId: number
  tp_trans: string
  qtde: number
  preco_ativo: number
  valor: number
  data: Date
  createdAt: Date
  updatedAt: Date
}

// eslint-disable-next-line @typescript-eslint/explicit-function-return-type
export default (sequelize: any, DataTypes: any) => {
  class Trade extends Model<TradeAttributes> implements TradeAttributes {
    id: number;
    AtivoId: number;
    CarteiraId: number;
    tp_trans: string;
    qtde: number;
    preco_ativo: number;
    valor: number;
    data: Date;
    createdAt: Date;
    updatedAt: Date;
    static associate (models: any): void {
      Trade.belongsTo(models.Ativo);
      Trade.belongsTo(models.Carteira);
    }
  }

  Trade.init({
    id: {
      type: DataTypes.BIGINT,
      allowNull: false,
      autoIncrement: true,
      primaryKey: true
    },
    AtivoId: {
      type: DataTypes.BIGINT,
      allowNull: false,
      references: { model: 'Ativo', key: 'id' },
      primaryKey: true
    },
    CarteiraId: {
      type: DataTypes.BIGINT,
      allowNull: false,
      references: { model: 'Carteira', key: 'id' },
      primaryKey: true
    },
    data: {
      type: DataTypes.DATE,
      allowNull: false,
      primaryKey: true
    },
    tp_trans: {
      type: DataTypes.STRING(1),
      allowNull: false
    },
    qtde: {
      type: DataTypes.DOUBLE,
      allowNull: false
    },
    preco_ativo: {
      type: DataTypes.DOUBLE,
      allowNull: false
    },
    valor: {
      type: DataTypes.DOUBLE,
      allowNull: false
    },
    createdAt: {
      type: DataTypes.DATE,
      allowNull: true
    },
    updatedAt: {
      type: DataTypes.DATE,
      allowNull: true
    }
  }, {
    sequelize,
    tableName: 'trades',
    modelName: 'Trade'
  });

  return Trade;
};
