export interface Ativo {
  id: number
  codigo?: string
  nome?: string
  preco?: number
  logo?: string
  twitter?: string
  stopLoss?: number
  marketCap?: number
  alvo?: number
  createdAt?: Date
  updatedAt?: Date
}
