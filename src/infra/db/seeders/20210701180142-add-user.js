'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.bulkInsert('usuarios', [{
      id: 1,
      nomeCompleto: 'Fabricio Piccinin',
      email: 'piccininsouza@gmail.com',
      usuario: 'fabricio ',
      senha: '123'
    }]);
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.bulkDelete('usuarios', null, {});
  }
};
