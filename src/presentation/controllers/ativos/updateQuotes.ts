import { Controller, HttpResponse } from '@/presentation/protocols';
import { serverError, ok } from '@/presentation/helpers';
import { AtivoUpdateQuotes } from '@/domain/protocols/ativos';

export class UpdateQuotesController implements Controller {
  constructor (
    readonly ativoUpdateQuotes: AtivoUpdateQuotes
  ) {}

  async handle (request: any): Promise<HttpResponse> {
    try {
      const result = await this.ativoUpdateQuotes.updateQuotes();
      return result ? ok({ updated: 'ok' }) : serverError(new Error('Internal Error'));
    } catch (error: any) {
      console.error(error);
      return serverError(error);
    }
  }
}
