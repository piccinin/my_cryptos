import { Controller, HttpResponse } from '@/presentation/protocols';
import { serverError, badRequest, ok } from '@/presentation/helpers';
import { MissingParamsError } from '@/presentation/errors/';
import { CarteiraServices } from '@/domain/services';

export class CreateCarteiraController implements Controller {
  constructor (
    readonly carteiraServices: CarteiraServices
  ) {}

  async handle (request: CreateCarteiraController.Request): Promise<HttpResponse> {
    try {
      for (const field of ['nome']) {
        if (!Object.prototype.hasOwnProperty.call(request, field)) {
          return badRequest(new MissingParamsError(field));
        }
      }
      const result = await this.carteiraServices.create(request);
      return result ? ok('ok') : serverError(new Error());
    } catch (error: any) {
      console.error(error);
      return serverError(error);
    }
  }
}

export namespace CreateCarteiraController {
  export type Request = {
    nome: string
    descricao?: string
    saldoDolar?: number
  };
}
