import { UsuarioServices } from '@/domain/services';
import { Usuario } from '@/domain/entities';
import { TokenPayload } from 'google-auth-library';
import { UsuarioLogin } from '@/domain/types';
import { getConfig } from '@/infra/config/config';
import jwt from 'jsonwebtoken';

export class LoginServices {
  private readonly authExpirationTime: number = 60 * 24 * 7; // em minutos

  public async login (ticket: TokenPayload, credential: string): Promise<string> {
    const usuarioServices = new UsuarioServices();
    const usuarios: Usuario[] = await usuarioServices.isRegistred(ticket.email);
    if (usuarios.length !== 1) {
      return '';
    }

    if (usuarios[0].ativo === 's') {
      const userData: UsuarioLogin = await usuarioServices.insertLogin(usuarios[0], ticket, credential, this.authExpirationTime);
      if (userData) {
        const userDataToReturn: Usuario = {
          id: usuarios[0].id,
          nomeCompleto: ticket.name,
          email: ticket.email,
          picture: ticket.picture,
          auth_token: userData.usuario.auth_token
        };
        const configs = await getConfig();
        const authJwt = jwt.sign(userDataToReturn,
          configs.config.jwtKey,
          {
            expiresIn: 60 * this.authExpirationTime
          });

        return authJwt;
      } else {
        return '';
      }
    } else {
      return '';
    }
  }
}
