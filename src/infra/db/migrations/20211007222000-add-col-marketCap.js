'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    return Promise.all([
      queryInterface.addColumn(
        'ativos',
        'marketCap',
        {
          type: Sequelize.DataTypes.DOUBLE,
          allowNull: true
        }
      )
    ]);
  },

  down: (queryInterface, Sequelize) => {
  }
};
