import axios from 'axios';
import Logger from '@/infra/logs/Logger';
import { CoinMarketCap } from '@/shared/types';

export class CoinMarketCapGateway {
  private readonly coinMC: CoinMarketCap;

  constructor (coinMC: CoinMarketCap) {
    this.coinMC = coinMC;
  }

  public async getCoinIdBySymbol (coinSimble: string): Promise<any> {
    try {
      const resp = await axios.get(this.coinMC.url + this.coinMC.coinMap, {
        headers: { [this.coinMC.headerKey]: this.coinMC.accessKey },
        params: { symbol: coinSimble }
      });
      return resp.data.data;
    } catch (err: any) {
      return err.response.status;
    }
  }

  public async getCoinInfoById (id: number): Promise<any> {
    try {
      const resp = await axios.get(this.coinMC.url + this.coinMC.coinInfo, {
        headers: { [this.coinMC.headerKey]: this.coinMC.accessKey },
        params: { id: id }
      });
      return resp.data.data;
    } catch (err: any) {
      return err.response.status;
    }
  }

  public async getCoinPriceBySymbol (coinSimble: string): Promise<string> {
    let resp: string = '';
    await axios.get(this.coinMC.url + this.coinMC.coinMap, {
      headers: { [this.coinMC.headerKey]: this.coinMC.accessKey },
      params: { symbol: coinSimble }
    }).then(function (response) {
      resp = response.data;
    });

    return resp;
  }

  public async loadCoinsPrices (coinsId: string): Promise<string> {
    let resp: string = '';
    await axios.get(this.coinMC.url + this.coinMC.coinQuotesLatest, {
      headers: { [this.coinMC.headerKey]: this.coinMC.accessKey },
      params: { id: coinsId }
    }).then(function (response) {
      resp = response.data;
    }).catch(function (error) {
      Logger.errorLog(error);
    });
    return resp;
  }

  public async loadCoinsTwitter (coinsId: string): Promise<string> {
    let resp: string = '';
    await axios.get(this.coinMC.url + this.coinMC.coinInfo, {
      headers: { [this.coinMC.headerKey]: this.coinMC.accessKey },
      params: { id: coinsId }
    }).then(function (response) {
      resp = response.data;
    }).catch(function (error) {
      Logger.errorLog(error);
    });

    return resp;
  }
}
