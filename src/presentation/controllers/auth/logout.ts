import { badRequest, ok, serverError, unauthorizedError } from '@/presentation/helpers';
import { Controller, HttpResponse } from '@/presentation/protocols';
import { UsuarioServices } from '@/domain/services';
import { MissingParamsError } from '@/presentation/errors';

export class LogoutController implements Controller {
  constructor (
    readonly usuarioServices: UsuarioServices
  ) {}

  async handle (request: LogoutController.Request): Promise<HttpResponse> {
    try {
      for (const field of ['auth_token']) {
        if (!Object.prototype.hasOwnProperty.call(request, field)) {
          return badRequest(new MissingParamsError(field));
        }
      }

      const logout: boolean = await this.usuarioServices.logout(request.auth_token);
      if (!logout) {
        return unauthorizedError('Usuário não cadastrado');
      }
      return ok(logout);
    } catch (error: any) {
      return serverError(error);
    }
  }
}

export namespace LogoutController {
  export type Request = {
    auth_token: string
  };
}
