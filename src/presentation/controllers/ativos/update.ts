import { Controller, HttpResponse } from '@/presentation/protocols';
import { serverError, badRequest, ok } from '@/presentation/helpers';
import { MissingParamsError } from '@/presentation/errors/';
import { UpdateService } from '@/domain/protocols';

export class UpdateAtivoController implements Controller {
  constructor (
    readonly updateServices: UpdateService
  ) {}

  async handle (request: UpdateAtivoController.Request): Promise<HttpResponse> {
    try {
      for (const field of ['id']) {
        if (!Object.prototype.hasOwnProperty.call(request, field)) {
          return badRequest(new MissingParamsError(field));
        }
      }
      const result = await this.updateServices.update(request);
      return result ? ok('ok') : serverError(new Error());
    } catch (error: any) {
      console.error(error);
      return serverError(error);
    }
  }
}

export namespace UpdateAtivoController {
  export type Request = {
    id: number
    codigo?: string
    nome?: string
    preco?: number
    logo?: string
    stopLoss?: number
    alvo?: number
  };
}
