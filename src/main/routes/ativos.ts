import { Application } from 'express';
import { adaptRoute } from '@/main/adapters/';
import {
  makeCreateAtivoController,
  makeUpdateAtivoController,
  makeDeleteAtivoController,
  makeFindAtivoController,
  makeUpdateQuotesController,
  makeTwitterController
} from '@/main/factories/controllers/makeAtivosControllers';
import {
  makeCoinDataController
} from '@/main/factories/controllers/makeCoinDataControllers';

export default async (app: Application): Promise<void> => {
  app.post('/api/ativo/getDataBySymbol', adaptRoute(await makeCoinDataController()));

  app.post('/api/ativo', adaptRoute(makeCreateAtivoController()))
    .patch('/api/ativo', adaptRoute(makeUpdateAtivoController()))
    .delete('/api/ativo', adaptRoute(makeDeleteAtivoController()));

  app.get('/api/ativo/listAll', adaptRoute(makeFindAtivoController('listAll')));
  app.get('/api/ativo/listAllIdName', adaptRoute(makeFindAtivoController('listAllIdName')));
  app.get('/api/ativo/updateQuotes', adaptRoute(makeUpdateQuotesController()));
  app.get('/api/ativo/getTweets', adaptRoute(makeTwitterController()));
};
