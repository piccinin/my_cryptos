import { Controller } from '@/presentation/protocols/controller';
import { LogoutController } from '@/presentation/controllers/auth';
import { UsuarioServices } from '@/domain/services';

export const makeLogoutController = (): Controller => {
  return new LogoutController(new UsuarioServices());
};
