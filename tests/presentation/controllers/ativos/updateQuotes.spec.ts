import { UpdateQuotesController } from "@/presentation/controllers/ativos";
import { AtivoServices } from '@/domain/services'

type SutTypes = {
  sut: UpdateQuotesController
  ativoServicesStub: AtivoServices
}

class AtivoServicesStub extends AtivoServices {
  public async updateQuotes(): Promise<any> {
      return false;
  }
}

const makeSut = (): SutTypes => {
  const ativoServicesStub = new AtivoServicesStub();
  const sut = new UpdateQuotesController(ativoServicesStub);
  return {
    sut,
    ativoServicesStub
  }
}

describe('UpdateAtivoController', () => {
  it('should return HTTP 500 if an internal server error happen', async () =>{
    const { sut, ativoServicesStub } = makeSut();

    jest.spyOn(ativoServicesStub, 'updateQuotes')
    .mockReturnValue(Promise.resolve(false));

    const httpResponse = await sut.handle('');

    expect(httpResponse.statusCode).toBe(500);
  })

  it('should return HTTP 200 if updateQuotes ativo service was corretly called', async () =>{
    const { sut, ativoServicesStub } = makeSut();

    jest.spyOn(ativoServicesStub, 'updateQuotes')
    .mockReturnValueOnce(Promise.resolve(true));

    const httpResponse = await sut.handle('');
    expect(httpResponse.statusCode).toBe(200);
  })


})