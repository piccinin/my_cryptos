'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.createTable('carteiras',{
      id: {
        type: Sequelize.DataTypes.BIGINT,
        allowNull: false,
        autoIncrement: true,
        primaryKey: true
      },
      nome: {
        type: Sequelize.DataTypes.STRING(50),
        allowNull: false
      },
      saldoDolar: {
        type: Sequelize.DataTypes.DOUBLE,
        allowNull: true
      },
      descricao: {
        type: Sequelize.DataTypes.STRING,
        allowNull: true
      },
      createdAt: {
        type: Sequelize.DataTypes.DATE,
        allowNull: true
      },
      updatedAt: {
        type: Sequelize.DataTypes.DATE,
        allowNull: true
      }
   });
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.dropTable('carteiras');
  }
};
