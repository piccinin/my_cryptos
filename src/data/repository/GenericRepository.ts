
export default class GenericRepository {
  private readonly model: any;
  private readonly sequelize: any;

  constructor (model: any, sequelize: any) {
    this.model = model;
    this.sequelize = sequelize;
  }

  public async create (entity: any): Promise<any> {
    return await this.model.create(entity);
  }

  public async delete (id: number): Promise<any> {
    const entity = await this.findById(id);
    return await entity.destroy();
  }

  public async update (entity: any): Promise<any> {
    return await this.model.update(entity, {
      where: { id: entity.id }
    });
  }

  public async findAll (): Promise<any[]> {
    return await this.model.findAll({
      order: this.sequelize.literal('nome ASC')
    });
  }

  public async findAllIdName (): Promise<any[]> {
    return await this.model.findAll({
      attributes: ['id', 'nome'],
      order: this.sequelize.literal('nome ASC')
    });
  }

  public async findById (id: number): Promise<any> {
    return await this.model.findByPk(id);
  }
}
