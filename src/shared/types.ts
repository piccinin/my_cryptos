export enum ENV {
  DEVELOPMENT = 'development',
  TESTING = 'testing',
  PRODUCTION = 'production'
}

export enum ERROR_TYPE{
  INFO = 'INFO',
  ERROR = 'ERROR'
}

export type CoinMarketCap = {
  readonly url: string
  readonly headerKey: string
  readonly accessKey: string
  readonly coinMap: string
  readonly coinInfo: string
  readonly coinQuotesLatest: string

};

export type TwitterConfigs = {
  readonly apiKey: string
  readonly apiKeySecret: string
  readonly bearerToken: string
};

export type Config = {
  readonly coinMarketCap: CoinMarketCap
  readonly twitterConfigs: TwitterConfigs
  readonly coinGecko: {}
  readonly jwtKey: string
};

export type Parameters = {
  readonly databases: Object
  readonly config: Config
};

export type UpdateSaldoFields = {
  id: number
  valor: number
  op: number
};
