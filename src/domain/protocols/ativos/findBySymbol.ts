import { Ativo } from '@/domain/entities';
export interface AtivoFindBySymbol {
  findBySymbol: (coinSymbol: string) => Promise<Ativo[]>
}
