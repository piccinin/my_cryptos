import { Controller, HttpResponse } from '@/presentation/protocols';
import { serverError, badRequest, ok } from '@/presentation/helpers';
import { MissingParamsError } from '@/presentation/errors/';
import { TradeServices } from '@/domain/services';

export class FindTradeController implements Controller {
  constructor (
    readonly tradeServices: TradeServices,
    readonly operacao: string
  ) {}

  async handle (request: FindTradeController.Request): Promise<HttpResponse> {
    try {
      switch (this.operacao) {
        case 'findAll': {
          const result = await this.tradeServices.findAll();
          return ok(result);
        }
        case 'findByAtivo': {
          const fieldsToValidate = ['ativoId'];
          for (const field of fieldsToValidate) {
            if (!Object.prototype.hasOwnProperty.call(request, field)) {
              return badRequest(new MissingParamsError(field));
            }
          }
          const result = await this.tradeServices.findByAtivoId(request.ativoId);
          return result ? ok(result) : serverError(new Error());
        }
        case 'getOverview': {
          const result = await this.tradeServices.getOverview();
          return ok(result);
        }
        case 'getSaldoDolarTrades': {
          const result = await this.tradeServices.getSaldoDolarTrades();
          return ok(result);
        }
        default: { throw new Error('Method not found'); }
      }
    } catch (error: any) {
      console.error(error);
      return serverError(error);
    }
  }
}

export namespace FindTradeController {
  export type Request = {
    ativoId: number
  };
}
