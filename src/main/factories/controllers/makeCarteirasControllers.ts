import { Controller } from '@/presentation/protocols/controller';

import {
  CreateCarteiraController,
  UpdateCarteiraController,
  DeleteCarteiraController,
  FindCarteiraController
} from '@/presentation/controllers/carteiras';

import { CarteiraServices } from '@/domain/services';

export const makeCreateCarteiraController = (): Controller => {
  return new CreateCarteiraController(new CarteiraServices());
};

export const makeUpdateCarteiraController = (): Controller => {
  return new UpdateCarteiraController(new CarteiraServices());
};

export const makeDeleteCarteiraController = (): Controller => {
  return new DeleteCarteiraController(new CarteiraServices());
};

export const makeFindCarteiraController = (operacao: string): Controller => {
  const constructorParams: FindCarteiraController.ContructorParams = {
    carteiraServices: new CarteiraServices(),
    operacao: operacao
  };
  return new FindCarteiraController(constructorParams);
};
