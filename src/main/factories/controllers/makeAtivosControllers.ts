import { Controller } from '@/presentation/protocols/controller';

import {
  CreateAtivoController,
  UpdateAtivoController,
  DeleteAtivoController,
  FindAtivoController,
  UpdateQuotesController,
  TwitterController
} from '@/presentation/controllers/ativos';

import {
  makeCreateServices,
  makeDeleteService,
  makeFindAllIdNameService,
  makeFindAllService
} from '@/main/factories/services/makeAtivoServices';

import { AtivoServices } from '@/domain/services';
import { TwitterGateway } from '@/infra/gateways/TwitterGateway';

export const makeCreateAtivoController = (): Controller => {
  return new CreateAtivoController(makeCreateServices());
};

export const makeUpdateAtivoController = (): Controller => {
  return new UpdateAtivoController(new AtivoServices());
};

export const makeDeleteAtivoController = (): Controller => {
  return new DeleteAtivoController(makeDeleteService());
};

export const makeUpdateQuotesController = (): Controller => {
  return new UpdateQuotesController(new AtivoServices());
};

export const makeTwitterController = (): Controller => {
  return new TwitterController(new TwitterGateway());
};

export const makeFindAtivoController = (operacao: string): Controller => {
  const constructorParams: FindAtivoController.ContructorParams = {
    findAllService: makeFindAllService(),
    findAllIdNameService: makeFindAllIdNameService(),
    operacao: operacao
  };
  return new FindAtivoController(constructorParams);
};
