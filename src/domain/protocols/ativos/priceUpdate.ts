export interface AtivoPriceUpdate {
  priceUpdate: (cmcStocksPrices: any) => Promise<void>
}
